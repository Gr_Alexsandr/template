import {Component, OnInit} from '@angular/core';
import {DataService} from '../services/data.service';
import {LogService} from '../services/log.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss'],
  providers: [DataService, LogService]
})
export class DataComponent implements OnInit {
  private items: string[] = [];
  private model = '';

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.items = this.dataService.getData();
  }

  addModel(model: string) {
    if (model) {
      this.dataService.addData(model);
      this.model = '';
    }
  }
}
