import {Component, OnInit} from '@angular/core';
import {HttpService} from '../services/http.service';
import {User} from './user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [HttpService]
})
export class UsersComponent implements OnInit {
  private users: User[] = [];
  private error: any;

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.httpService.getUsers().subscribe(
      (users: User[]) => this.users = users,
      error => {
        this.error = error.message;
        console.log(error);
      }
    );
  }

}
