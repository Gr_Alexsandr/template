import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-life-cycle-child',
  templateUrl: './life-cycle-child.component.html',
  styleUrls: ['./life-cycle-child.component.scss']
})
export class LifeCycleChildComponent implements OnInit, OnChanges {
  @Input() name: string;
  @Input() age: string;

  one = false;
  two = true;
  five = false;
  count = 2;

  constructor() {
    this.log(`constructor`);
  }

  ngOnInit() {
    this.log(`onInit`);
  }

  ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line:forin
    for (const propName in changes) {
      const chng = changes[propName];
      const cur = JSON.stringify(chng.currentValue);
      const prev = JSON.stringify(chng.previousValue);
      this.log(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
    }
  }

  private log(msg: string) {
    console.log(msg);
  }

  onSwitch(num: number) {
    this.count = num;
    if (num === 1) {
      this.one = true;
      this.two = false;
      this.five = false;
    } else if (num === 2) {
      this.one = false;
      this.two = true;
      this.five = false;
    } else if (num === 5) {
      this.one = false;
      this.two = false;
      this.five = true;
    }
  }
}
