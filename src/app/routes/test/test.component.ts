import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent {
  name = 'Bob';
  age = '25';
  brend = 'Sony';
  prise = '500$';
  nik = 'Rex';

  clicks = 0;
  changed(increased: any) {
    increased ? this.clicks++ : this.clicks--;
  }
}
