import {Component, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-life-cycle',
  templateUrl: './life-cycle.component.html',
  styleUrls: ['./life-cycle.component.scss']
})
export class LifeCycleComponent implements OnChanges {
  isVerdana = true;
  isNavy = true;
  currentClasses = {
    verdanaFont: this.isVerdana,
    navyColor: this.isNavy
  };
  visibility = true;

  name = 'Tom';
  age = 25;

  ngOnChanges(changes: SimpleChanges) {
    // tslint:disable-next-line:forin
    for (const propName in changes) {
      const chng = changes[propName];
      const cur = JSON.stringify(chng.currentValue);
      const prev = JSON.stringify(chng.previousValue);
      this.log(`${propName}: currentValue = ${cur}, previousValue = ${prev}`);
    }
  }

  private log(msg: string) {
    console.log(msg);
  }

  toggle() {
    this.visibility = !this.visibility;
  }
}
