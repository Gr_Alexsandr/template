import {Directive, ElementRef, Renderer2} from '@angular/core';

@Directive({
  selector: '[appFontSize]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '(mouseenter)': 'onMouseEnter()',
    '(mouseleave)': 'onMouseLeave()'
  }
})
export class FontSizeDirective {
  constructor(private element: ElementRef, private renderer: Renderer2) {

    this.renderer.setStyle(this.element.nativeElement, 'cursor', 'pointer');
  }

  onMouseEnter() {
    this.setFontWeight('24px');
  }

  onMouseLeave() {
    this.setFontWeight('16px');
  }

  private setFontWeight(val: string) {
    this.renderer.setStyle(this.element.nativeElement, 'font-size', val);
  }
}
