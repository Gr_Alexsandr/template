import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appRed]'
})
export class RedDirective {
  private color = 'black';

  @HostBinding('style.color') get getFontWeight() {

    return this.color;
  }

  @HostBinding('style.cursor') get getCursor() {
    return 'pointer';
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.color = 'red';
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.color = 'black';
  }
}
