import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent {
  @Input() userName;
  @Input() userAge;
  @Input() brend;
  @Output() changed = new EventEmitter<boolean>();
  @Input() nik;
  @Output() nikChange = new EventEmitter<string>();

  name = 'Tom';
  colspan = 2;
  count = 0;
  isRed = false;
  prise: string;

  onClick($event: any, flag: boolean) {
    console.log($event);
    flag ? this.count++ : this.count--;
  }

  change(increased: any) {
    this.changed.emit(increased);
  }

  onNikChange(model: string) {
    this.nik = model;
    this.nikChange.emit(model);
  }

  @Input()
  set inputPrise(brend: string) {
    if (brend === 'Panasonic') {
      this.prise = '550$';
    } else if (brend === 'Samsung') {
      this.prise = '350$';
    } else {
      this.prise = '500$';
    }
  }

  get inputPrise() {
    return this.prise;
  }
}
