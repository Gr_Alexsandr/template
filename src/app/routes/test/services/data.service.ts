import {Injectable, Optional} from '@angular/core';
import {LogService} from './log.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private data: string[] = ['Apple', 'Nokia', 'Samsung'];

  constructor(@Optional() private logService: LogService) {
  }

  getData(): string[] {
    if (this.logService) {
      this.logService.write('операция получения данных');
    }
    return this.data;
  }

  addData(brend: string) {
    this.data.push(brend);
    if (this.logService) {
      this.logService.write('операция добавления данных');
    }
  }
}
