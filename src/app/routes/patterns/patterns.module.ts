import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {PatternsComponent} from './patterns.component';

const routes: Routes = [
  { path: 'patterns', component: PatternsComponent}
];

@NgModule({
  declarations: [PatternsComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class PatternsModule { }
