export class Todo {
  id?: string;
  title: string;
  time: Date;
  done: boolean;
  edit: boolean;
  show: boolean;

  constructor(data?: Todo) {
    if (data) {
      this.id = data.id;
      this.title = data.title;
      this.time = data.time;
      this.done = data.done;
      this.edit = data.edit;
      this.show = data.show;
    }
  }

}
