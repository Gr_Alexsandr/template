import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {TodoListComponent} from './todo-list.component';

const routes: Routes = [
  { path: 'todo', component: TodoListComponent}
  ];


@NgModule({
  declarations: [TodoListComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class TodoListModule {
}
