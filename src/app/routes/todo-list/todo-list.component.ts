import {Component, OnInit} from '@angular/core';
import { HttpService } from './services/http.service';
import { Todo } from './todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  providers: [HttpService]
})
export class TodoListComponent implements OnInit {
  edit = false;
  title = '';
  todoList = [];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getTodoList().subscribe(
      todoList => this.todoList = todoList
    );
  }

  getIndex(id: number) {
    return this.todoList.findIndex(t => t.id === id);
  }

  showAllContent(id: number) {
    const index = this.getIndex(id);
    const todo = this.todoList[index].show;
    this.todoList[index].show = !todo;
  }

  switchToEdit(id: number) {
    const index = this.getIndex(id);
    this.todoList[index].edit = true;
  }

  addTodo(title: string) {
    if (title) {
      const todo = {
        title,
        time: new Date(),
        done: false,
        edit: false,
        show: false
      };
      this.title = '';
      this.httpService.addTodo(new Todo(todo)).subscribe(
        t => this.todoList.push(t)
      );
    }
  }

  todoIsDone(id: number) {
    const index = this.getIndex(id);
    this.todoList[index].done = !this.todoList[index].done;
    this.httpService.updateTodoList(id, this.todoList[index]).subscribe(
      todo => this.todoList[index] = todo,
      error => {
        console.error(error);
      }
    );
  }

  deleteTodo(id: number) {
    this.httpService.deleteTodo(id).subscribe(
      todo => this.todoList = this.todoList.filter(t => todo.id !== t.id),
      error => {
        console.error(error);
      }
    );
  }

  updateTodo(id: number, newTitle: string) {
    const index = this.getIndex(id);
    if (this.todoList[index].edit) {
      this.todoList[index].title = newTitle;
      this.todoList[index].edit = false;
      this.todoList[index].show = false;
      this.httpService.updateTodoList(id, this.todoList[index]).subscribe(
        todo => this.todoList[index] = todo,
        error => {
          console.error(error);
        }
      );
    }
  }
}
