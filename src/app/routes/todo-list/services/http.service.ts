import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Todo} from '../todo';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  url = 'https://5b1a476c83b6190014ca3a81.mockapi.io/todolist';

  constructor(private http: HttpClient) { }

  getTodoList(): Observable<Todo[]> {
    return this.http
      .get<Todo[]>(`${this.url}`);
  }

  addTodo(todo: Todo): Observable<Todo> {
    return this.http
      .post<Todo>(`${this.url}`, todo);
  }

  deleteTodo(id: number): Observable<Todo> {
    return this.http
      .delete<Todo>(`${this.url}/${id}`);
  }

  updateTodoList(id: number, body): Observable<Todo> {
    return this.http
      .put<Todo>(`${this.url}/${id}`, body);
  }
}
