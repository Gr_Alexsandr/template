import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';

import {TodoListModule} from './routes/todo-list/todo-list.module';
import {PatternsModule} from './routes/patterns/patterns.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildComponent } from './routes/test/child/child.component';
import { LifeCycleComponent } from './routes/test/life-cycle/life-cycle.component';
import { LifeCycleChildComponent } from './routes/test/life-cycle-child/life-cycle-child.component';
import { BoldDirective } from './routes/test/directives/bold.directive';
import { RedDirective } from './routes/test/directives/red.directive';
import { FontSizeDirective } from './routes/test/directives/font-size.directive';
import { DataComponent } from './routes/test/data/data.component';
import { UsersComponent } from './routes/test/users/users.component';
import { HomeComponent } from './routes/home/home.component';
import { TestComponent } from './routes/test/test.component';
import { NotFoundComponent } from './routes/not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'todo', loadChildren: './routes/todo-list/todo-list.module#TodoListModule'},
  { path: 'patterns', loadChildren: './routes/patterns/patterns.module#PatternsModule'},
  { path: 'test', component: TestComponent},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,
    LifeCycleComponent,
    LifeCycleChildComponent,
    BoldDirective,
    RedDirective,
    FontSizeDirective,
    DataComponent,
    UsersComponent,
    HomeComponent,
    TestComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    TodoListModule,
    PatternsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
